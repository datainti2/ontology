import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OntologyComponent } from './src/ontology.component';

export * from './src/ontology.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    OntologyComponent,

  ],
  exports: [
    OntologyComponent
  ]
})
export class OntologyModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: OntologyModule,
      providers: []
    };
  }
}
